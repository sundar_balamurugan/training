import React from 'react';

class form extends React.Component{
    render(){
        return(
            
            <div>
            <Form onSubmit={this.add}>
                <Form.Group as={Row} controlId="formHorizontalName">
                    <Form.Label column sm={2} className="mx-sm-3">
                        Book_Name
                    </Form.Label>
                        <Col sm={5}>
                            <Form.Control type="text" placeholder="Book_name"  name="bookname" value={this.state.bookname} 
                            onChange={this.handle}/>
                        </Col>
                </Form.Group>{this.state.booknameerror}

                <Form.Group as={Row}>
                    <Form.Label column sm={2} className="mx-sm-3">
                        upload image
                    </Form.Label>
                        <Col sm={5}>
                            <Form.File id="exampleFormControlFile1" className="mx-sm-3" name="image" value={this.state.image} 
                            onChange={this.handleim}/>
                        </Col>
                </Form.Group>{this.state.imageerror}

                <Form.Group as={Row} controlId="formHorizontalCode">
                    <Form.Label column sm={2} className="mx-sm-3">
                        Book_code
                    </Form.Label>
                        <Col sm={5}>
                            <Form.Control type="text" placeholder="book_code" name="bookcode" value={this.state.bookcode}
                            onChange={this.handlecd}/>
                        </Col>
                </Form.Group>{this.state.bookcodeerror}

                <Form.Group as= {Row} controlId="exampleForm.ControlSelect1">
                    <Form.Label column sm={2} className="mx-sm-3">
                         Category
                    </Form.Label>
                        <Col sm={5}>
                            <Form.Control as="select" default="Choose..." name="category" value={this.state.category} 
                            onChange={this.handlect}>
                                <option>Choose...</option>
                                <option>Fantasy</option>
                                <option>Horror</option>
                                <option>comic</option>
                                <option>adventure</option>
                            </Form.Control>
                        </Col>
                </Form.Group>{this.state.categoryerror}

                <Form.Group as={Row} controlId="formHorizontalCost">
                    <Form.Label column sm={2} className="mx-sm-3">
                        cost
                    </Form.Label>
                        <Col sm={5}>
                            <Form.Control type="text" placeholder="Cost" name="cost" value={this.state.cost} 
                            onChange={this.handlecs} />
                        </Col>
                </Form.Group>{this.state.costerror}

                <Form.Group as={Row} controlId="dob">
                    <Form.Label column sm={2} className="mx-sm-3">
                        Published At
                    </Form.Label>
                        <Col sm={5}>
                            <Form.Control type="date" placeholder="Published At" name="published" value={this.state.published}
                            onChange={this.handlepb}/>
                        </Col>
                </Form.Group>{this.state.publishederror}

                <fieldset>
                <Form.Group as={Row}>
                    <Form.Label as="legend" column sm={2} className="mx-sm-3">
                        Stock_inventory
                    </Form.Label >
                        <Col sm={5}>
                            <Row>
                            <Form.Check
                                type="radio"
                                label="stock_in"
                                name="stock"
                                id="formHorizontalRadios1"
                                className="mx-sm-3"
                                checked={this.state.stock=="stock_in"}
                                onChange={this.handlest}
                            />
                            <Form.Check
                                type="radio"
                                label="stock_out"
                                name="stock"
                                id="formHorizontalRadios2"
                                className="mx-sm-3"
                                checked={this.state.stock=="stock_out"}
                                onChange={this.handlest}
                            />
                            </Row>
                        </Col>
                </Form.Group>{this.state.stockerror}
                </fieldset>

                <Form.Group as={Row} controlId="formHorizontal_author_name">
                    <Form.Label column sm={2} className="mx-sm-3">
                            Author_Name
                    </Form.Label>
                        <Col sm={5}>
                            <Form.Control type="text" placeholder="Author_name" name="authorname" value={this.state.authorname} 
                            onChange={this.handlean}/>
                        </Col>
                </Form.Group>{this.state.authornameerror}

                <Form.Group as={Row}>
                    <Col sm={{ span: 10, offset: 2 }}>
                        <Button type="submit">Add Book</Button>
                    </Col>
                </Form.Group>
            </Form>
        </div>
        )
    }
}