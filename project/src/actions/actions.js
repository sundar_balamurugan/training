import axios from "axios";
import { APIURL } from '../utils/Constants';

export const setflag = (flag) => {
  return {
    type: 'SET_FLAG',
    flag
  }
};

export const fetchBook = (book) => {
  return {
    type: 'FETCH_BOOK',
    payload: book
  }
};

export const fetchBooks = () => {
  return (dispatch) => {
    dispatch(setflag(true))
    return axios.get(APIURL + '/book1')
      .then(response => {
        dispatch(fetchBook(response.data))
        dispatch(setflag(false))
      })
      .catch(error => {
        throw (error);
      })
  }
};

export const createBooks = (book) => {
  return (dispatch) => {
    dispatch(setflag(true))
    return axios.post(APIURL + '/book1', book)
      .then(response => {
        dispatch(setflag(false))
      })
      .catch(error => {
        throw (error)
      })
  }
};

export const editBook = (id, book) => {
  return (dispatch) => {
    dispatch(setflag(true))
    return axios.put(APIURL + '/book1/' + id, book)
      .then(response => {
        dispatch(setflag(false))
      })
      .catch(error => {
        throw (error)
      })
  }
};

export const deletebook = (id, book) => {
  return (dispatch) => {
    dispatch(setflag(true))
    return axios.delete(APIURL + '/book1/' + id, book)
  }
};

export const getbooks = () => {
  return (dispatch) => {
    dispatch(setflag(true))
    return axios.get(APIURL + '/Books')
      .then(response => {
        dispatch(fetchBook(response.data))
        dispatch(setflag(false))
      })
  }
};

export const login = (credentials) => {
  return (dispatch) => {
    dispatch(setflag(true))
    return axios.post(APIURL + '/login', credentials)
      .then(response => {
        localStorage.setItem('islogin', true);
        dispatch(setflag(false))
      })
  }
};

export const favbook = (id, book) => {
  return (dispatch) => {
    return axios.put(APIURL + '/Books/' + id, book)
      .then((response) => { })
  }
};

