import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Table, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { fetchBooks, deletebook } from '../../actions/actions';
import LinkRouter from './Header';
import Loader from '../../utils/Loader';
import { withRouter } from 'react-router';

class ListBook extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Books: [],
        }
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
    }

    componentDidMount() {
        this.props.fetchBooks();
    }

    delete(index, detail) {
        this.props.deletebook(index, detail);
        const timer = setTimeout(() => {
            this.props.fetchBooks();
        }, 1000);
    }

    edit(index) {
        this.props.history.push(`/editbook/${index}`);
    }

    render() {
        const { Books } = this.props;
        return (
            <div>
                <Loader />
                <header className="navbar">
                    <h1 className="headertitle">Book Admin</h1>
                </header>
                <LinkRouter />
                <h3 className="tabletitle">Book Details</h3>
                <Container>
                    <Table>
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Book Name</th>
                                <th>Image</th>
                                <th>Book Code</th>
                                <th>Category</th>
                                <th>Cost</th>
                                <th>Published</th>
                                <th>Stock Inventory</th>
                                <th>Author Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {Books && Books.map((detail, index) =>
                                <tr key={index}>
                                    <td>{++index}</td>
                                    <td>{detail.bookname}</td>
                                    <td>{detail.image}</td>
                                    <td>{detail.bookcode}</td>
                                    <td>{detail.category}</td>
                                    <td>{detail.cost}</td>
                                    <td>{detail.published}</td>
                                    <td>{detail.radiobox}</td>
                                    <td>{detail.authorname}</td>
                                    <td><Button variant="primary" onClick={(e) => this.edit(detail.id)}>
                                        Edit</Button>{' '}
                                        <Button variant="danger" onClick={(e) => this.delete(detail.id, detail)}>
                                            Delete</Button>
                                    </td>
                                </tr>
                            )}
                        </tbody>
                    </Table>
                </Container>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        Books: state.bookReducer.books
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchBooks: () => { dispatch(fetchBooks()) },
        deletebook: (index, detail) => { dispatch(deletebook(index, detail)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ListBook));