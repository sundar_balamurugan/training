import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { createBooks, editBook } from '../../actions/actions'
import { Button } from 'react-bootstrap';

class Add extends React.Component {
    constructor(props) {
        super(props);
        let { bookname, image, bookcode, category, cost, published, radiobox, authorname } = this.props.Book || {};
        this.state = {
            bookname: bookname || '',
            image: image || '',
            bookcode: bookcode || '',
            category: category || '',
            cost: cost || '',
            published: published || '',
            radiobox: radiobox || '',
            authorname: authorname || '',
            booknameerror: '',
            imageerror: '',
            bookcodeerror: '',
            categoryerror: '',
            costerror: '',
            publishederror: '',
            stockerror: '',
            authornameerror: '',
        };
        this.handle = this.handle.bind(this);
        this.add = this.add.bind(this);
        this.edit = this.edit.bind(this);
        this.validating = this.validating.bind(this);
    }

    handle(e) {
        if (e.target) {
            this.setState({
                [e.target.name]: e.target.value,
            })
        }
    }

    validating() {
        let booknameerror = '';
        let imageerror = '';
        let bookcodeerror = '';
        let categoryerror = '';
        let costerror = '';
        let publishederror = '';
        let stockerror = '';
        let authornameerror = '';
        let RegEx = /^[a-z0-9]+$/i;
        let crct = (RegEx.test(this.state.bookcode));

        if (!isNaN(this.state.authorname)) {
            authornameerror = 'author name should not be numeric';
        }

        if (!isNaN(this.state.bookname)) {
            booknameerror = 'book name should not be numeric';
        }

        if (!crct) {
            bookcodeerror = 'Bookcode should be alphanumeric';
        }

        if (!this.state.bookname) {
            booknameerror = 'Enter the book-name';
        }

        if (!this.state.image) {
            imageerror = 'Upload the image';
        }

        if (!this.state.bookcode) {
            bookcodeerror = 'Enter the code';
        }

        if (!this.state.category) {
            categoryerror = 'Choose the category';
        }

        if (isNaN(this.state.cost)) {
            costerror = 'cost should be numeric';
        }

        if (!this.state.cost) {
            costerror = 'Enter the cost';
        }

        if (!this.state.published) {
            publishederror = 'Enter the published date';
        }

        if (!this.state.radiobox) {
            stockerror = 'choose the stock';
        }

        if (!this.state.authorname) {
            authornameerror = 'Enter the author name';
        }

        if (booknameerror || bookcodeerror || imageerror || categoryerror || costerror ||
            publishederror || stockerror || authornameerror) {
            this.setState({
                booknameerror,
                bookcodeerror,
                imageerror,
                categoryerror,
                costerror,
                publishederror,
                authornameerror,
                stockerror
            })
            return false;
        }
        return true;
    }

    add(v) {
        v.preventDefault();
        const valid = this.validating();
        if (valid) {
            const { bookname,
                image,
                bookcode,
                category,
                cost,
                published,
                radiobox,
                authorname } = this.state;
            const books = {
                bookname, image, bookcode, category, cost, published, radiobox, authorname
            };
            this.props.createBooks(books)
            const timer = setTimeout(() => {
                this.props.history.push('/listbook')
            }, 1000)
        }
    }

    edit(v) {
        v.preventDefault();
        const { bookname,
            image,
            bookcode,
            category,
            cost,
            published,
            radiobox,
            authorname } = this.state;
        const books = {
            bookname, image, bookcode, category, cost, published, radiobox, authorname
        };
        const index = this.props.Book.id;
        this.props.editBook(index, books)
        const timer = setTimeout(() => {
            this.props.history.push('/listbook')
        }, 1000)
    }

    render() {
        let button;
        let submit;
        let imageedit = '';
        let imagelabel = '';
        const { booknameerror,
            imageerror,
            bookcodeerror,
            categoryerror,
            costerror,
            publishederror,
            stockerror,
            authornameerror } = this.state;

        if (!this.props.Book) {
            imagelabel = <p className="label">Image:</p>
            imageedit = <input type="file" className="field" name="image"
                onChange={(e) => this.handle(e)} className="forminput"
                placeholder='upload image' />
            button = <Button variant="primary" className="addbookbutton"
                type='submit'>Add</Button>;
            submit = (v) => this.add(v);
        }
        else {
            imageedit = <p className="label">Image: book.jpg</p>;
            button = <Button variant="primary" className="addbookbutton"
                type='submit'>Edit</Button>;
            submit = (v) => this.edit(v);
        }

        return (
            <form onSubmit={(v) => submit(v)}>
                <table className="addbookform">
                    <tbody>
                        <tr>
                            <td className="formtitle">Book Details</td>
                        </tr>
                        <tr>
                            <td>
                                <p className="label">BookName:</p>
                                <input type="text" name='bookname' value={this.state.bookname}
                                    onChange={(e) => this.handle(e)} className="forminput"
                                    placeholder='Enter book name' />
                                <p className="error">{booknameerror}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    {imagelabel}
                                    {imageedit}
                                </div>
                                <p className="error">{imageerror}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className="label">Book Code:</p>
                                <input type="text" className="forminput" name="bookcode"
                                    value={this.state.bookcode} onChange={(e) => this.handle(e)}
                                    placeholder='Enter book code' />
                                <p className="error">{bookcodeerror}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className="label">Category:</p>
                                <select name="category" value={this.state.category}
                                    onChange={(e) => this.handle(e)} className="forminput">
                                    <option defaultValue="choose one.." name="category">choose one..</option>
                                    <option value="fantasy" name="category"  >Fantasy</option>
                                    <option value="horror" name="category"  >Horror</option>
                                    <option value="comic" name="category"  >comic</option>
                                    <option value="adventure" name="category"  >Adventure</option>
                                    <option value="thriller" name="category"  >Thriller</option>
                                    <option value="children" name="category"  >Children</option>
                                    <option value="science" name="category"  >Science</option>
                                </select>
                                <p className="error">{categoryerror}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className="label">Cost:</p>
                                <input type="text" name="cost" className="forminput"
                                    value={this.state.cost} onChange={(e) => this.handle(e)}
                                    placeholder='Enter cost' />
                                <p className="error">{costerror}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className="label">published At:</p>
                                <input type="date" name="published" className="forminput"
                                    value={this.state.published} onChange={(e) => this.handle(e)} />
                                <p className="error">{publishederror}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className="label">Stock_inventory:</p>
                                <input type="radio" value="stockin" className="radiobutton"
                                    name="radiobox" checked={this.state.radiobox == "stockin"}
                                    onChange={(e) => this.handle(e)} />In-stock

                        <input type="radio" value="stockout" className="radiobutton"
                                    name="radiobox" checked={this.state.radiobox == "stockout"}
                                    onChange={(e) => this.handle(e)} />Out-of-stock
                        <p className="error">{stockerror}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className="label">AuthorName:</p>
                                <input type="text" className="forminput" name="authorname"
                                    value={this.state.authorname} onChange={(e) => this.handle(e)}
                                    placeholder='Enter Author name' />
                                <p className="error">{authornameerror}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colSpan='2'>
                                {button}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createBooks: (books) => { dispatch(createBooks(books)) },
        editBook: (index, books) => { dispatch(editBook(index, books)) }
    }
}

export default connect(null, mapDispatchToProps)(withRouter(Add));