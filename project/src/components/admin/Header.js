import React from 'react';
import { Link, Route } from "react-router-dom";
import { Button } from "react-bootstrap";
import { logout } from '../../utils/Authentication';
import { withRouter } from 'react-router'

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.handlelogout = this.handlelogout.bind(this);
    }

    handlelogout() {
        logout();
        this.props.history.push('/login');
    }

    render() {
        return (
            <Route>
                <Button className="buttonlink" onClick={(e) => { this.handlelogout(e) }}>
                    Logout</Button>
                <Link to='/listbook' className="Link1">List Book</Link>
                <Link to='/add' className="Link1">Add Book</Link>
                <Link to='/editbook/:id' className="Link1"></Link>
            </Route>
        )
    }
}

export default (withRouter(Header));