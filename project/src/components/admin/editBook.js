import React, { useState } from 'react';
import Add from './Add';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import { useEffect } from 'react';

export default function EditBook(props) {
  let { id } = useParams();
  const [book, setbook] = useState(null);
  useEffect(() => {
    axios.get(`https://607d0e6c184368001769cdd4.mockapi.io/book1/${id}`)
      .then((res) => {
        setbook(res.data);
      });
  }, []);

  if (book)
    return (<Add Book={book} />);
  else
    return (<h1></h1>)
}

