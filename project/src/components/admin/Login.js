import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { login } from '../../actions/actions'
import { withRouter } from 'react-router';
import Loader from '../../utils/Loader';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            usernameerror: '',
            passworderror: '',
        }
        this.handlesubmit = this.handlesubmit.bind(this);
        this.handlechange = this.handlechange.bind(this);
        this.validating = this.validating.bind(this);
    }

    handlechange(v) {
        if (v.target) {
            this.setState({
                [v.target.name]: v.target.value
            })
        }
    }

    validating() {
        let usernameerror = '';
        let passworderror = '';
        if (!this.state.username) {
            usernameerror = 'Enter the username';
        }
        if (!this.state.password) {
            passworderror = 'Enter the password';
        }

        if (this.state.username != 'sundar') {
            usernameerror = 'incorrect username';
        }

        if (this.state.password != 'sundar') {
            passworderror = 'incorrect password';
        }

        if (usernameerror || passworderror) {
            this.setState({ usernameerror, passworderror })
            return false;
        }
        return true;
    }

    handlesubmit(v) {
        v.preventDefault();
        const valid = this.validating();
        if (valid) {
            const { username,
                password } = this.state;
            const credentials = {
                username,
                password
            }
            this.props.login(credentials)
            const timer = setTimeout(() => {
                this.props.history.push(`/listbook`)
            }, 2000)
        }
    }

    render() {
        const { usernameerror,
            passworderror } = this.state
        return (
            <form>
                <table className="adminform">
                    <tbody>
                        <tr>
                            <td colSpan='2'><p className="formtitle">Login</p></td>
                        </tr>
                        <tr>
                            <td>
                                <p className="label">User name</p>
                                <input name="username" value={this.state.username}
                                    onChange={(v) => { this.handlechange(v) }}
                                    className="forminput" placeholder="Enter username" />
                                <p className="error">{usernameerror}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className="label">Password</p>
                                <input type='password' name="password" value={this.state.password}
                                    onChange={(v) => { this.handlechange(v) }}
                                    className="forminput" placeholder="Enter password" />
                                <p className="error">{passworderror}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Button type='primary' className="formbutton"
                                    onClick={(v) => { this.handlesubmit(v) }}>Login</Button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <Loader />
            </form>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        login: (credentials) => { dispatch(login(credentials)) }
    }
}

export default connect(null, mapDispatchToProps)(withRouter(Login));