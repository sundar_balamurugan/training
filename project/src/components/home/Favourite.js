import React from 'react';
import { Table, Button, Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import { favbook, getbooks } from '../../actions/actions';
import Loader from '../../utils/Loader';

class Favourite extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
        this.removefavourite = this.removefavourite.bind(this);
    }

    componentDidMount() {
        this.props.getbooks();
    }

    removefavourite(book) {
        book.isfavourite = false;
        const id = book.id;
        this.props.favbook(id, book);
        const timer = setTimeout(() => {
            this.props.getbooks();
        }, 1000);
    }

    render() {
        const favbook = this.props.book ? this.props.book.filter((book) => {
            return book.isfavourite
        }) : [];

        return (
            <div >
                <h1 className="bookheader">Favourite Book</h1>
                <Container>
                    <Loader />
                    <Table className="favouritebody">
                        <tbody>
                            {favbook.length === 0 ?
                                <tr>
                                    <td colSpan='2'>
                                        <h3 className="favheader">No favourites</h3>
                                    </td>
                                </tr> :
                                favbook && favbook.map((book, index) => <tr key={index}  >
                                    <td><img alt={book.bookname} src={book.bookimage} /></td>
                                    <td>
                                        <div className="bookdetails">
                                            <p><strong>Book Name</strong>: {book.bookname}</p>
                                            <p><strong>Author Name</strong>: {book.authorname}</p>
                                            <p><strong>Category</strong>: {book.category}</p>
                                            <p><strong>Price</strong>: Rs.{book.cost}</p>
                                            <Button variant="danger" >Buy now</Button>{' '}
                                            <Button variant="warning" onClick={() => this.removefavourite(book)}>
                                                Remove</Button>
                                        </div>
                                    </td></tr>
                                )
                            }
                        </tbody>
                    </Table>
                </Container>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        book: state.bookReducer.books
    }
}

function mapDispatchToProps(dispatch) {
    return {
        favbook: (id, book) => { dispatch(favbook(id, book)) },
        getbooks: () => { dispatch(getbooks()) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Favourite)