import React from 'react';
import { Button } from 'react-bootstrap';

export default class Review extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            review: ''
        }
        this.handleReview = this.handleReview.bind(this);
        this.handlesubmit = this.handlesubmit.bind(this);
    }

    handleReview(val) {
        if (val.target) {
            this.setState({
                [val.target.name]: val.target.value
            })
        }
    }

    handlesubmit(val) {
        val.preventDefault();
        const Review = this.state.review;
        this.setState({ review: '', Review })
    }

    render() {
        return (
            <div>
                <div className="bookdetail">
                    <p><strong>Review</strong> : {this.state.Review}</p>
                </div>
                <p className="viewreview">
                    <input type="text" className="view" name='review'
                        value={this.state.review}
                        onChange={(v) => this.handleReview(v)}
                        placeholder="Review comments" />
                    <Button variant="dark" className="subbutton"
                        onClick={(v) => { this.handlesubmit(v) }}>
                        submit</Button>
                </p>
            </div>
        )
    }
}
