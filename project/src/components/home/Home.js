import { Button, Form, FormControl, NavDropdown, Nav, Navbar } from "react-bootstrap";
import { connect } from "react-redux";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Table } from "react-bootstrap";
import React, { Component } from 'react';
import { getbooks } from '../../actions/actions';
import Loader from "../../utils/Loader";
import ReactPaginate from 'react-paginate';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            books: [],
            filteredbooks: [],
            resetbooks: [],
            pagebooks: [],
            isfilter: false,
            isreset: false,
            search: '',
            offset: 0,
            perpage: 4,
            currentpage: 0
        }
        this.initialvalue = undefined;
        this.handlefilter = this.handlefilter.bind(this);
        this.handleDetail = this.handleDetail.bind(this);
        this.handlesearch = this.handlesearch.bind(this);
        this.handlereset = this.handlereset.bind(this);
        this.fetchbook = this.fetchbook.bind(this);
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    componentDidMount() {
        this.props.getbooks();
    }

    componentDidUpdate(prevProps) {
        if ((this.state.books.length === 0) && (!this.state.isfilter)) {
            const books = this.props.books;
            const count = Math.ceil(books.length / this.state.perpage)
            this.setState({ books: books, pagecount: count }, () => this.fetchbook())
        }
    }

    handlePageChange(e) {
        let selectedpage = e.selected;
        let offset = selectedpage * this.state.perpage;
        this.setState({ currentpage: selectedpage, offset }, () => this.fetchbook())
    }

    handlereset() {
        const books = this.props.books;
        const count = Math.ceil(books.length / this.state.perpage)
        this.setState({
            resetbooks: books,
            isreset: true,
            pagecount: count
        }, () => this.fetchbook()
        )
    }

    handlefilter(value, low, high) {
        let books;
        if (value || this.initialvalue) {
            this.initialvalue = value || this.initialvalue;
            books = this.state.books.filter((book) => book.category === this.initialvalue);
        }

        if (high || low) {
            if (this.initialvalue) {
                books = books.filter((books) => books.cost >= low && books.cost <= high)
            }
            else {
                books = this.state.books.filter((books) => books.cost >= low && books.cost <= high)
            }
        }
        const count = Math.ceil(books.length / this.state.perpage)
        this.setState({
            filteredbooks: books,
            isfilter: true,
            pagecount: count
        },
            () => this.fetchbook()
        )
    }

    handlesearch(search) {
        let books;
        books = this.state.books.filter((data) => {
            if (data.bookname.toLowerCase().includes(search.toLowerCase())) {
                return data;
            }
        })
        const count = Math.ceil(books.length / this.state.perpage)
        this.setState({
            filteredbooks: books,
            isfilter: true,
            pagecount: count
        },
            () => this.fetchbook())
    }

    handleDetail(book) {
        this.props.history.push(`/detail/${book.id}`)
    }

    fetchbook() {
        if (this.state.isfilter) {
            const pagebooks = this.state.filteredbooks
                .slice(this.state.offset, this.state.offset + this.state.perpage)
            this.setState({ pagebooks })
        }
        else {
            const pagebooks = this.state.books
                .slice(this.state.offset, this.state.offset + this.state.perpage)
            this.setState({ pagebooks })
        }
    }
    render() {
        const books = this.state.pagebooks;

        return (
            <div className="homebody">
                <h1 className="bookheader">Online Books Purchase</h1>
                <Container>
                    <Navbar bg="light" variant="light">
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                <NavDropdown title="Category" id="basic-nav-dropdown">
                                    <NavDropdown.Item onClick={() => this.handlefilter('Horror')}>
                                        Horror</NavDropdown.Item>
                                    <NavDropdown.Item onClick={() => this.handlefilter('Fantasy')}>
                                        Fantasy</NavDropdown.Item>
                                    <NavDropdown.Item onClick={() => this.handlefilter('Thriller')}>
                                        Thriller</NavDropdown.Item>
                                    <NavDropdown.Item onClick={() => this.handlefilter('Mystery')}>
                                        Mystery</NavDropdown.Item>
                                    <NavDropdown.Item onClick={() => this.handlefilter('Craft')}>
                                        Craft</NavDropdown.Item>
                                </NavDropdown>
                                <NavDropdown title="Price" id="basic-nav-dropdown">
                                    <NavDropdown.Item onClick={() => this.handlefilter(undefined, 250, 500)}>
                                        250-500</NavDropdown.Item>
                                    <NavDropdown.Item onClick={() => this.handlefilter(undefined, 500, 1000)}>
                                        500-1000</NavDropdown.Item>
                                </NavDropdown>
                            </Nav>
                            <Form inline>
                                <FormControl type="text" placeholder="Search by bookname" className="mr-sm-2"
                                    onChange={(e) => { this.handlesearch(e.target.value) }} />{''}
                                <Button type="submit" onClick={(e) => this.handlereset(e)}>Reset</Button>
                            </Form>
                        </Navbar.Collapse>
                    </Navbar>
                    <Table className="booktable">
                        <tbody>
                            {books.map((book, index) => <tr key={index}  >
                                <td><img alt={book.bookname} src={book.bookimage} /></td>
                                <td>
                                    <div className="bookdetails">
                                        <p><strong>Book Name</strong>: {book.bookname}</p>
                                        <p><strong>Author Name</strong>: {book.authorname}</p>
                                        <p><strong>Category</strong>: {book.category}</p>
                                        <p><strong>Price</strong>: Rs.{book.cost}</p>
                                        <Button variant="danger" >Buy now</Button>{' '}
                                        <Button variant="success" onClick={() => this.handleDetail(book)} >
                                            Detailed View</Button>
                                    </div>
                                </td></tr>
                            )}
                        </tbody>
                    </Table>
                    <Container>
                        <div className='footer' >
                            <ReactPaginate
                                previousLabel={"prev"}
                                nextLabel={"next"}
                                breakLabel={"..."}
                                breakClassName={"break-me"}
                                pageCount={this.state.pagecount}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={3}
                                onPageChange={this.handlePageChange}
                                containerClassName={"pagination"}
                                subContainerClassName={"pages pagination"}
                                activeClassName={"active"} />
                        </div>
                    </Container>
                    <Loader />
                </Container>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        books: state.bookReducer.books
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getbooks: () => { dispatch(getbooks()) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);