import { connect } from 'react-redux';
import React from 'react';
import { Button, Container, Table } from 'react-bootstrap';
import { favbook } from '../../actions/actions';
import Loader from '../../utils/Loader';
import axios from 'axios';
import Review from '../home/Review';

class Detail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            book: {},
            review: '',
            isfav: false
        }
        this.handlefavourite = this.handlefavourite.bind(this);
    }

    handlefavourite(book) {
        book.isfavourite = true;
        const id = book.id;
        this.props.favbook(id, book);
        this.setState({ isfav: true })
        alert('Added to favourite');
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get(`https://607d0e6c184368001769cdd4.mockapi.io/Books/${id}`)
            .then((res) => {
                const book = res.data
                this.setState({ book });
            })
    }

    render() {
        const { book } = this.state;
        let button;
        if (!this.state.isfav) {
            button = <Button variant="warning" className="button"
                onClick={() => this.handlefavourite(book)}>
                Add to favourite</Button>
        }

        else {
            button = <Button variant="warning" className="button"
                disabled='true'>Added to favourite</Button>
        }

        return (
            <div>
                <Loader />
                <h2 className="bookheader">{book.bookname}</h2>
                <Container>
                    <Table className="detailbooktable">
                        <tbody>
                            <tr>
                                <td>
                                    <img className="image" src={book.bookimage} alt={book.bookname} />
                                </td>
                                <td>
                                    <div className="bookdetail">
                                        <p><strong>Book Name</strong> : {book.bookname}</p>
                                        <p><strong>Author Name</strong> : {book.authorname}</p>
                                        <p><strong>Category</strong> : {book.category}</p>
                                        <p><strong>Cost</strong> : Rs.{book.cost}</p>
                                        <p><strong>Description</strong> : {book.description}</p>
                                    </div>
                                    <p className="viewbutton">
                                        <Button variant="danger" className="button">
                                            Buy Now</Button>{' '}
                                        {button}
                                    </p>
                                    <Review />
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </Container>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        favbook: (id, book) => { dispatch(favbook(id, book)) }
    }
}

export default connect(null, mapDispatchToProps)(Detail);