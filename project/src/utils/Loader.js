import React from 'react';
import loader from '../utils/loader.gif';
import { connect } from 'react-redux';

class Loader extends React.Component {
    render() {
        const loading = this.props.flag

        if (!loading) {
            return null;
        }
        return (
            <div className="loader-container">
                <div className="loader" >
                    <img src={loader} alt="loading.." />
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        flag: state.flagReducer.flag
    }
}

export default connect(mapStateToProps, null)(Loader);