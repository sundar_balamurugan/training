import axios from "axios";
import { APIURL2 } from './Constants';

export const login = (credentials) => {
  return axios.post(APIURL2, credentials)
    .then(response => {
      localStorage.setItem('islogin', true);
    })
}

export const islogin = () => {
  if (localStorage) {
    return localStorage.getItem('islogin')
  }
}

export const logout = () => {
  if (localStorage.getItem('islogin')) {
    localStorage.removeItem('islogin')
  }
}
