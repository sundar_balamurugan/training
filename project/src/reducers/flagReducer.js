import { initialvalue } from '../utils/Constants';

const flagReducer = (state = initialvalue, action) => {
    switch (action.type) {
        case 'SET_FLAG':
            const flag = action.flag
            return {
                flag
            }

        default: return state
    }
}

export default flagReducer;