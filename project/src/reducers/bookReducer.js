const bookReducer = (state = {}, action) => {

    switch (action.type) {
        case 'FETCH_BOOK':
            return {
                state,
                books: action.payload
            }

        default: return state;
    }
}

export default bookReducer;