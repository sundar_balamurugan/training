import React, { Component } from 'react';
import { Route, Switch, Router } from "react-router-dom";
import Add from '../components/admin/Add';
import EditBook from '../components/admin/editBook';
import Login from '../components/admin/Login';
import Home from '../components/home/Home';
import ListBook from '../components/admin/ListBook';
import Detail from '../components/home/Detail';
import History from "./History";
import PrivateRoute from '../Routers/PrivateRoute';
import Favourite from '../components/home/Favourite';

class MainRouter extends React.Component {
    render() {
        return (
            <Router history={History} >
                <Switch>
                    <Route exact path='/' component={Home}></Route>
                    <PrivateRoute path='/listbook' component={ListBook} />
                    <PrivateRoute path='/add' component={Add} />
                    <PrivateRoute path='/editbook/:id' component={EditBook} />
                    <Route path='/login' component={Login}></Route>
                    <Route path='/detail/:id' component={Detail}></Route>
                    <Route path='/favourite' component={Favourite}></Route>
                </Switch>
            </Router>
        )
    }
}

export default MainRouter;