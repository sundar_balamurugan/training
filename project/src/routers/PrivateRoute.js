import React from 'react';
import { Redirect, Route } from "react-router";
import { islogin } from '../utils/Authentication'

const PrivateRoute = ({ component: Component, path: path }) => {
    return (
        <Route path={path} render={props => (
            islogin() ?
                <Component {...props} /> :
                <Redirect to='/login' />
        )} />
    )
}

export default PrivateRoute;