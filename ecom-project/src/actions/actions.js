import axios from "axios"

export const login = (loginCredentials) => {
    return (dispatch) => {
        return axios.post(`https://607d0e6c184368001769cdd4.mockapi.io/loginecommerce`, loginCredentials)
            .then((res) => { })
    }
}

export const fetchproducts= (products) => {
    return{
        type: 'FETCH PRODUCTS',
        payload: products
    }
}

export const getProducts= () => {
    return (dispatch)=> {
        return axios.get(`https://60cad89c21337e0017e43279.mockapi.io/products`)
        .then(res => {
            dispatch( fetchproducts(res.data));
        })
    }
}