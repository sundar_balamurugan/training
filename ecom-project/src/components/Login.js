import React from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { login } from '../actions/actions';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            usernameerror: '',
            passworderror: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.validate = this.validate.bind(this);
    }
    handleChange(v) {
        if (v.target) {
            this.setState({
                [v.target.name]: v.target.value
            })
        }
    }

    validate() {
        let usernameerror = '';
        let passworderror = '';
        if (!this.state.username) {
            usernameerror = 'username should not be empty';
        }
        if (!this.state.password) {
            passworderror = 'password should not be empty';
        }
        if (usernameerror || passworderror) {
            this.setState({
                usernameerror, passworderror
            })
            return false;
        }
        return true;
    }

    handleLogin(v) {
        v.preventDefault();
        const valid = this.validate();
        if (valid) {
            const { username, password } = this.state;
            const loginCredential = { username, password };
            this.props.login(loginCredential);
            alert('login succesfull');
        }

    }

    render() {
        const { usernameerror, passworderror } = this.state;
        return (
            <form>
                <table className="loginform">
                    <tbody>
                        <tr>
                            <td colSpan='2'><p className='formtitle'>Login</p></td>
                        </tr>
                        <tr>
                            <td>
                                <p className="label">Username</p>
                                <input name="username" value={this.state.username} placeholder="Username"
                                    className="input" onChange={(v) => { this.handleChange(v) }} />
                                <p className="error">{usernameerror}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p className="label">password</p>
                                <input name="password" value={this.state.password} placeholder="password"
                                    className="input" onChange={(v) => { this.handleChange(v) }} />
                                <p className="error">{passworderror}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <Button type='primary' className="button" onClick={(v) => { this.handleLogin(v) }}>
                                    Login</Button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        login: (loginCredential) => { dispatch(login(loginCredential)) }
    }
}

export default connect(null, mapDispatchToProps)(Login);