import React from 'react';

class Home extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getProducts();
    }

    render() {
        return (
            <div></div>
        )
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        products: state
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getProducts: () => { dispatch(getProducts()) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);