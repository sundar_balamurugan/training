function add(x,y){
 return x+y   
}

console.log(add(100,200));

//Evaluating default parameter
function put(toy, toyBox = []) {
    toyBox.push(toy);
    return toyBox;
}

console.log(put('Toy Car'));
console.log(put('Teddy Bear'));

//function to return date
function date(d = today()) {
    console.log(d);
}
function today() {
    return (new Date()).toLocaleDateString("en-US");
}
date();