//Resolve
var promise = Promise.resolve(4234);
promise.then( val=>{
    console.log(val)
})

//Reject
function main(){
    return new Promise(function(resolve,reject){
        setTimeout(() => {
            const error=true;
            reject();

        }, 2000);
    });
}
main().catch(function(){
    console.log("Rejected the promises")
});