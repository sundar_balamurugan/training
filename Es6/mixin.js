let sayHiMixin = {
    sayHi() {
       console.log(`Hello ${this.name}`);
    },
    sayBye() {
       console.log(`Bye ${this.name}`);
    }
 };
 class Person {
    constructor(name) {
       this.name = name;
    }
 }
 
 Object.assign(Person.prototype, sayHiMixin);
 new Person("John").sayHi();