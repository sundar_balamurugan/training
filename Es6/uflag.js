var text = "𠮷";

console.log(text.length);           
console.log(/^.$/.test(text));      
console.log(/^.$/u.test(text));

function codePointLength(text) {
    var result = text.match(/[\s\S]/gu);
    return result ? result.length : 0;
}

console.log(codePointLength("abc"));    
console.log(codePointLength("𠮷bc"));