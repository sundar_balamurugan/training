const form = document.getElementById('event-registration-form');

const validationError = document.getElementById('error');

const alphabetsOnlyPattern = /^[a-zA-Z]+$/;

const emailPattern = /^[a-z_0-9]+[@]{1}[aspiresys]+[.][com]$/;

    form.onsubmit = function(event) {
        
        event.preventDefault();

        let firstName = document.getElementById('firstname');

        let lastName = document.getElementById('lastname');

        let companyName = document.getElementById('company_name');

        let email = document.getElementById('email');

        let phoneNumber = document.getElementById('phone_No');

        let subject = document.getElementById('subject');

        let errorMessage = '';

        if(firstName.value == '') {

            errorMessage = 'Your first name is required !';

            firstName.focus();

        } else if(!alphabetsOnlyPattern.test(firstName.value)) {

            errorMessage = 'Please enter valid alphabets on your first name !';

            firstName.focus();

        } else if(lastName.value == '') {

            errorMessage = 'Your last name is required !';

            lastName.focus();

        } else if(!alphabetsOnlyPattern.test(lastName.value)) {

            errorMessage = 'Please enter valid alphabets on your last name !';

            lastName.focus();

        } else if(companyName.value == '') {

            errorMessage = 'Your company name is required !';

            companyName.focus();

        } else if(!alphabetsOnlyPattern.test(companyName.value)) {

            errorMessage = 'Please enter valid alphabets on your company name !';

            companyName.focus();

        } else if(email.value == '') {

            errorMessage = 'Your email id is required !';

            email.focus();

        } else if(!emailPattern.test(email.value)) {

            errorMessage = 'Please enter valid email id !';

            email.focus();

        } else if(phoneNumber.value == '') {

            errorMessage = 'Your phone number is required !';

            phoneNumber.focus();

        } else if(isNaN(phoneNumber.value)) {

            errorMessage = 'Please enter valid phone number !';

            phoneNumber.focus();

        } else if(phoneNumber.value.length != 10) {

            errorMessage = 'Phone number must contain 10 digits only !';

            phoneNumber.focus();

        } else if(subject.value == '') {

            errorMessage = 'Please enter subject !';

            subject.focus();

        } else if(!alphabetsOnlyPattern.test(subject.value)) {

            errorMessage = 'Please enter alphabets on subject !';

            subject.focus();

        }

        if(errorMessage != '') {

            validationError.innerHTML = errorMessage;

            validationError.style.display = 'block';

        } else {

            validationError.innerHTML = '';

            validationError.style.display = 'none';

            form.reset();

            alert("Registration success ! Thank you :)");

        }

    }